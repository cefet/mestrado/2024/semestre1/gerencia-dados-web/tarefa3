Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)

Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Gerência de Dados da Web

Professores: Thiago Magela Rodrigues Dias e Gray Farias Moita

Estudante: André Almeida Rocha

---

# Tarefa 03 – Expressões Regulares

![Expressões Regulares logo](images/regex.svg 'Expressões Regulares')

## Instalação do projeto
### Criar ambiente virtual:
```sh
python3 -m venv .venv
```
Referência: [Criando ambientes virtuais](https://docs.python.org/pt-br/dev/library/venv.html#creating-virtual-environments)


### Ativar o ambiente virtual:
```sh
source .venv/bin/activate
```
Referência: [Como funcionam os venvs](https://docs.python.org/pt-br/dev/library/venv.html#how-venvs-work)

### Instalar dependências do projeto:
```sh
pip3 install .
```

## Execução do jupyter notebook
```sh
jupter-lab tarefa3/AndreRocha_expressoes-regulares.ipynb
```

O navegador padrão deve abrir no seguinte endereço:
[http://localhost:8888/lab/tree/AndreRocha_expressoes-regulares.ipynb](http://localhost:8888/lab/tree/AndreRocha_expressoes-regulares.ipynb)

### Parar a execução do servidor Jupyter
No terminal dar o comando para cancelar e confirmar:
```sh
ctrl+c
```

### Desativar o ambiente virtual
```sh
deactivate
```

> Esse comando independe do sistema operacional.
